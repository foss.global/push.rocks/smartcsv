/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartcsv',
  version: '2.0.2',
  description: 'handle csv data | gitzone standard compliant'
}
